import React from 'react';

const imgSrc = 'https://img-ssl.vatera.hu/rating/stars/gold.png';

export default class ItemView extends React.Component {
  handleClick() {
    alert('hello');
  }

  render() {
    const { mennyiseg, jelenlegiNyertes } = this.props;
    // const mennyiseg = this.props.mennyiseg;
    return (
      <>
        <div className="row">
          <div className="col-5">Mennyiség</div>
          <div className="col-7">{mennyiseg}</div>
        </div>
        <div className="row">
          <div className="col-5">Jelenlegi nyertes</div>
          <div className="col-7" onClick={this.handleClick}>
            {jelenlegiNyertes.username}({jelenlegiNyertes.numberOfTransactions})
            {
            jelenlegiNyertes.isProfessional
              ? <img src={imgSrc} alt="" />
              : <span />
            }
          </div>
        </div>
        <div className="row">
          <div className="col-5">stb...</div>
          <div className="col-7"/>
        </div>
      </>
    );
  }
}
