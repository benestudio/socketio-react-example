import React, { Component } from 'react';
import './App.css';
import ItemView from './ItemView';
import io from 'socket.io-client';

class App extends Component {
  constructor() {
    super();
    this.state = {
      mennyiseg: '1 db',
      jelenlegiNyertes: {
        username: 'akeeka',
        numberOfTransactions: 123,
        isProfessional: false,
      },
    };
    const socket = io('http://localhost:3000');
    socket.on('bidder-updated', (user) => {
      console.log('bidder-updated', user);
      this.updateBidder(user);
    });
  }

  updateBidder({
    username,
    numberOfTransactions,
    isProfessional,
  }) {
    this.setState({
      jelenlegiNyertes: {
        username,
        numberOfTransactions,
        isProfessional,
      },
    });
  }

  render() {
    const { mennyiseg, jelenlegiNyertes } = this.state;
    return (
      <div className="App">
        <ItemView
          mennyiseg={mennyiseg}
          allapot={'Használt'}
          jelenlegiNyertes={jelenlegiNyertes}
        />
      </div>
    );
  }
}

export default App;
